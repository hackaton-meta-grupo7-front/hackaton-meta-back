import { BaseDatabase } from "./BaseDatabase";
import Avaliation from "../model/Avaliation";
export default class AvaliationData extends BaseDatabase {
  protected AVALIATION_TABLE_NAME = "meta_leaguers_avaliations";

  public insertAvaliation = async (avaliation: Avaliation) => {
    try {
      await this.connection(this.AVALIATION_TABLE_NAME).insert(avaliation);

      return avaliation;
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  public selectAvaliationById = async (avaliation_id: string) => {
    try {
      const result = await this.connection(this.AVALIATION_TABLE_NAME)
        .select("*")
        .where({ avaliation_id });

      return {
        avaliation: result[0],
      };
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  public avaliationExists = async (avaliation_id: string) => {
    try {
      const result = await this.connection(this.AVALIATION_TABLE_NAME)
        .select("*")
        .where({ avaliation_id });

      return result.length > 0;
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  public selectAvaliationsByLeaguerName = async (professional_name: string) => {
    try {
      const avaliations = await this.connection(this.AVALIATION_TABLE_NAME)
        .select("*")
        .where({ professional_name });
      return avaliations;
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

}
