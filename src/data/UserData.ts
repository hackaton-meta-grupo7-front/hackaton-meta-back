import User from "../model/User";
import Leaguer from "../model/Leaguer";
import { BaseDatabase } from "./BaseDatabase";
import { LeaguerInputDTO } from "../types/leaguerInputDTO";

export default class UserData extends BaseDatabase {
  protected TABLE_NAME = "meta_users_id";
  protected LEAGUER_TABLE_NAME = "meta_leaguers_profile";
  protected AVALIATIONS_TABLE_NAME = "meta_leaguers_avaliations";

  insertUser = async (user: User) => {
    try {
      await this.connection(this.TABLE_NAME).insert(user);
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  findByEmail = async (email: string) => {
    try {
      const queryResult: any = await this.connection(this.TABLE_NAME)
        .select()
        .where({ email });

      return queryResult[0];
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  findByName = async (name: string) => {
    try {
      const queryResult: any = await this.connection(this.TABLE_NAME)
        .select()
        .where({ name });

      return queryResult[0];
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  updateUserRole = async (id: string, role: string) => {
    try {
      await this.connection(this.TABLE_NAME)
        .update({ role })
        .where({ id });
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  insertLeaguer = async (leaguer: Leaguer) => {
    try {
      await this.connection(this.LEAGUER_TABLE_NAME).insert(leaguer);
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  findLeaguerById = async (leaguer_id: string) => {
    try {
      const queryResult: any = await this.connection(this.LEAGUER_TABLE_NAME)
        .select()
        .where({ leaguer_id });

      return queryResult[0];
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  getLeaguers = async () => {
    try {
      return this.connection(this.LEAGUER_TABLE_NAME).select("*");
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  getLeaguersByResponsibleName = async (responsibleName: string) => {
    try {
      return this.connection(this.LEAGUER_TABLE_NAME)
        .select("*")
        .where({ responsibleName });
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  selectLeaguerWithAvaliations = async (
    leaguer_id: string,
    professional_name: string
  ) => {
    try {
      const queryResult: any = await this.connection(this.LEAGUER_TABLE_NAME)
        .select("*")
        .where({ leaguer_id });

      const leaguer = queryResult[0];

      const queryResultAvaliations: any = await this.connection(
        this.AVALIATIONS_TABLE_NAME
      )
        .select("*")
        .where({ professional_name })
        .orderBy("created_at", "desc");

      leaguer.avaliation = queryResultAvaliations;

      return leaguer;
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };

  updateLeaguer = async (leaguer_id: string, input: LeaguerInputDTO) => {
    try {
      await this.connection(this.LEAGUER_TABLE_NAME)
        .update(input)
        .where({ leaguer_id });
    } catch (error) {
      if (error instanceof Error) {
        throw new Error(error.message);
      } else {
        throw new Error("Erro do banco !");
      }
    }
  };
}
