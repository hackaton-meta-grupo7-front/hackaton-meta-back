import AvaliationProcess from "../model/AvaliationProcess";
import { BaseDatabase } from "./BaseDatabase";

export default class AvaliationProcessData extends BaseDatabase {
    protected TABLE_NAME = "meta_avaliation_processes"
    
    

    public insert = async (avaliation: AvaliationProcess) => {
        try {
            await this.connection(this.TABLE_NAME).insert(avaliation);
        } catch (error) {
            if (error instanceof Error) {
                throw new Error(error.message);
            } else {
                throw new Error("Erro do banco !");
            }
        }
    }

    public selectAvaliationProcess = async (userId: string, leaguerId: string) => {
        try {

            const queryResult: any = await this.connection(this.TABLE_NAME)
                .select()
                .where({ leaguer_id: leaguerId });
            return queryResult;
        } catch (error) {
            if (error instanceof Error) {
                throw new Error(error.message);
            } else {
                throw new Error("Erro do banco !");
            }
        }
    }

    public checkIfAvaliationProcessExists = async (userId: string, leaguerId: string) => {
        try {
            const queryResult: any = await this.connection(this.TABLE_NAME)
                .select()
                .where({ leaguer_id: leaguerId });
            if(queryResult.length > 0){
                return true;
            }
            return false;
        } catch (error) {
            if (error instanceof Error) {
                throw new Error(error.message);
            } else {
                throw new Error("Erro do banco !");
            }
        }
    }
    
}