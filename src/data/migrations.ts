import { BaseDatabase } from "./BaseDatabase";

class Migrations extends BaseDatabase {
  public createTables = async (): Promise<void> => {
    try {
      await this.connection.raw(`
      CREATE TABLE IF NOT EXISTS  meta_users_id (
        id VARCHAR(255) PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        username VARCHAR(255) NOT NULL UNIQUE,
        password VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL UNIQUE,
        role VARCHAR(64) NOT NULL
      );
      
      CREATE TABLE IF NOT EXISTS meta_leaguers_profile(
        leaguer_id VARCHAR(255) PRIMARY KEY,
        leaguerName VARCHAR(255) NOT NULL,
        leaguerEmail VARCHAR(255) NOT NULL UNIQUE,
        leaguerClass ENUM('TURMA_PILOTO', 'TURMA_1', 'TURMA_2', 'TURMA_3') NOT NULL,
        step ENUM('INTRODUCAO', 'LABS', 'BETA') NOT NULL,
        creatorId VARCHAR(255) NOT NULL,
        responsibleName VARCHAR(45) NOT NULL,
        FOREIGN KEY (responsibleId) REFERENCES meta_users_id(id)
        );

      CREATE TABLE IF NOT EXISTS meta_avaliation_processes(
        id VARCHAR(255) PRIMARY KEY,
        people_involved_emails VARCHAR(255) NOT NULL,
        leaguer_id VARCHAR(255) NOT NULL,
        responsible_id VARCHAR(255) NOT NULL,
        colleagues VARCHAR(255) NOT NULL,
        client_id VARCHAR(255) DEFAULT 'clientDefaultId' NOT NULL,
        date DATE NOT NULL,
        avaliations VARCHAR(600),
        FOREIGN KEY (leaguer_id) REFERENCES meta_leaguers_profile(id),
        FOREIGN KEY (responsible_id) REFERENCES meta_users_id(id)
        );
      
        
      CREATE TABLE IF NOT EXISTS meta_leaguers_avaliations( 
          avaliation_id VARCHAR(255) PRIMARY KEY NOT NULL,
          avaliation_by VARCHAR(255) NOT NULL,  
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          time_in_program VARCHAR(20) ,
          professional_firstname  VARCHAR(100),
          professional_lastname  VARCHAR(100),
          performance_unique VARCHAR(10) , 
          performance_comment VARCHAR(255) , 
          delivered_unique VARCHAR(10) , 
          delivered_comment VARCHAR(255) , 
          proativity_unique VARCHAR(10) , 
          proativity_comment VARCHAR(255) , 
          commitment_unique VARCHAR(10) , 
          commitment_comment VARCHAR(255) , 
          teamplayer_unique VARCHAR(10) , 
          teamplayer_comment VARCHAR(255) , 
          developedskills_unique VARCHAR(10) , 
          developedskills_comment VARCHAR(255) , 
          leardshipskills_unique VARCHAR(10) , 
          leardshipskills_comment VARCHAR(255) , 
          pontuality_unique VARCHAR(10) , 
          pontuality_comment VARCHAR(255) , 
          workingunderpression_unique VARCHAR(10) , 
          workingunderpression_comment VARCHAR(255) , 
          engagementleasonlearned_unique VARCHAR(10) , 
          engagementleasonlearned_comment VARCHAR(255) , 
          administrative_activities_unique VARCHAR(10) , 
          administrative_activities_comment VARCHAR(255) ,
          professional_caracteristics_highlights VARCHAR(2000) ,
          general_considerations VARCHAR(2000),
          technology VARCHAR(255),
          projects VARCHAR(45),
        );
      `);

      console.log("Tabelas criadas com sucesso");
      await this.connection.destroy();
    } catch (error: any) {
      console.log(error.sqlMessage || error.message);
      await this.connection.destroy();
    }
  };
}

new Migrations().createTables();
