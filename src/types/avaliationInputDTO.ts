

export type AvaliationInputDTO = {
  avaliation_by: string;
  projects: any;
  technologies: any;
  time_in_program: string;
  professional_name: string;
  performance_unique: string;
  performance_comment: string;
  delivered_unique: string;
  delivered_comment: string;
  proativity_unique: string;
  proativity_comment: string;
  commitment_unique: string;
  commitment_comment: string;
  teamplayer_unique: string;
  teamplayer_comment: string;
  developedskills_unique: string;
  developedskills_comment: string;
  leardshipskills_unique: string;
  leardshipskills_comment: string;
  pontuality_unique: string;
  pontuality_comment: string;
  workingunderpression_unique: string;
  workingunderpression_comment: string;
  engagementleasonlearned_unique: string;
  engagementleasonlearned_comment: string;
  administrative_activities_unique: string;
  administrative_activities_comment: string;
  professional_caracteristics_highlights: string;
  general_considerations: string;
};
