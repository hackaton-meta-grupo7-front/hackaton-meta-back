
export type AvaliationProcessInputDTO = {
    people_involved_emails: string
    leaguer_id: string
    colleagues: string
}