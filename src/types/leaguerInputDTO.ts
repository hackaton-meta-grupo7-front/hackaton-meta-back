import { LeaguerClass, LeaguerStep } from "../model/Leaguer"

export type LeaguerInputDTO = {
    responsibleName: string,
    leaguerName: string
    leaguerEmail: string
    leaguerClass: LeaguerClass
    step: LeaguerStep
}