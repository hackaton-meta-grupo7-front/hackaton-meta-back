export type authenticationData = {
    id: string
    role: string
    name: string
}