import { UserRole } from '../model/User';

export type SignupInputDTO = {
    name: string
    username: string
    email: string
    password: string
    role: UserRole
}