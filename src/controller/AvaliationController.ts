import { Request, Response } from "express";
import AvaliationBusiness from "../business/AvaliationBusiness";
import { AvaliationInputDTO } from "../types/avaliationInputDTO";

export default class AvaliationController {
  constructor(private avaliationBusiness: AvaliationBusiness) {}

  createAvaliation = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const avaliation_id = req.body.avaliation_id;
      const input = req.body as AvaliationInputDTO;

      const avaliation = await this.avaliationBusiness.createAvaliationForm(
        avaliation_id,
        input,
        token
      );
      res.status(200).send({ avaliation });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao criar avaliação");
    }
  };

  getAvaliation = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const avaliation_id = req.params.avaliation_id;
      const result = await this.avaliationBusiness.getAvaliationById(
        avaliation_id,
        token
      );
      res.status(200).send({ result });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao buscar avaliação");
    }
  }
}