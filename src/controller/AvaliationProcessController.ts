import AvaliationProcessBusiness from "../business/AvaliationProcessBusiness";
import { AvaliationProcessInputDTO } from "../types/avaliationProcessInputDTO";
import { Request, Response } from "express";

export default class AvaliationProcessController {
  constructor(private avaliationProcessBusiness: AvaliationProcessBusiness) {}

  createAvaliationProcess = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const input = req.body as AvaliationProcessInputDTO;
    
      const process =
        await this.avaliationProcessBusiness.createAvaliationProcess(
          input,
          token
        );
      res.status(200).send({ process });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao criar processo de avaliação");
    }
  };

  viewAvaliationProcesses = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const leaguerId = req.body.leaguerId;
      const avaliation_process =
        await this.avaliationProcessBusiness.viewAvaliationProcesses(
          token,
          leaguerId
        );
      res.status(200).send({ avaliation_process });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao visualizar avaliações");
    }
  };

  checkIfAvaliationProcessExists = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const leaguerId = req.body.leaguerId;
      const result =
        await this.avaliationProcessBusiness.checkIfAvaliationProcessExists(
          token,
          leaguerId
        );
      res.status(200).send({ result });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao verificar se existe processo de avaliação");
    }
  };
}
