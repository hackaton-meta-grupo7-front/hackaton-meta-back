import { Request, Response } from "express";
import UserBusiness from "../business/UserBusiness";
import { SignupInputDTO } from "../types/signupInputDTO";
import { LoginInputDTO } from "../types/loginInputDTO";
import { LeaguerInputDTO } from "../types/leaguerInputDTO";
import { UserRole } from "../model/User";
import { tokenexpiration } from "../services/Authenticator";

export default class UserController {
  constructor(private userBusiness: UserBusiness) {}

  signup = async (req: Request, res: Response) => {
    const { name, username, email, password, role } = req.body;

    const input: SignupInputDTO = {
      name,
      username,
      email,
      password,
      role,
    };
    try {
      const token = await this.userBusiness.signup(input);
      res
        .status(201)
        .send({ message: "Usuário cadastrado com sucesso", token });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro no signup");
    }
  };

  login = async (req: Request, res: Response) => {
    const { email, password } = req.body;

    const input: LoginInputDTO = {
      email,
      password,
    };

    try {
      const token = await this.userBusiness.login(input);
      res.status(200).send({ message: "Usuário logado com sucesso", token });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro no login");
    }
  };

  createLeaguer = async (req: Request, res: Response) => {
    const token = req.headers.authorization as string;
    const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } =
      req.body;

    const input: LeaguerInputDTO = {
      responsibleName,
      leaguerName,
      leaguerEmail,
      leaguerClass,
      step,
    };

    try {
      await this.userBusiness.createLeaguer(input, token);
      res.status(201).send({ message: "Leaguer criado com sucesso!" });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao criar Leaguer");
    }
  };

  editLeaguers = async (req: Request, res: Response) => {
    const token = req.headers.authorization as string;

    const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } =
      req.body;

    const leaguer_id: string = req.params.leaguer_id;

    const input: LeaguerInputDTO = {
      responsibleName,
      leaguerName,
      leaguerEmail,
      leaguerClass,
      step,
    };

    try {
      await this.userBusiness.editLeaguers(leaguer_id, input, token);
      res.status(201).send({ message: "Leaguer editado com sucesso!" });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao editar Leaguer");
    }
  };

  editUserRole = async (req: Request, res: Response) => {
    const token = req.headers.authorization as string;

    const role: UserRole = req.body.role;

    const user_name: string = req.body.user_name;

    try {
      await this.userBusiness.editUserRole(user_name, role, token);
      res.status(201).send({ message: "Role editado com sucesso!" });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao editar Role");
    }
  };

  getLeaguers = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const leaguers = await this.userBusiness.getLeaguers(token);
      res.status(200).send({ leaguers });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao buscar Leaguers");
    }
  };

  getLeaguerById = async (req: Request, res: Response) => {
    const token = req.headers.authorization as string;
    const leaguer_id: string = req.params.leaguer_id;
    try {
      const leaguer = await this.userBusiness.getLeaguerById(leaguer_id, token);
      res.status(200).send({ leaguer });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao buscar Leaguer");
    }
  };

  showTokenPayload = async (req: Request, res: Response) => {
    try {
      const token = req.headers.authorization as string;
      const result = await this.userBusiness.showTokenPayload(token);
      res.status(200).send({ result });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao mostrar payload do token");
    }
  };

  changeTokenExpiration = async (req: Request, res: Response) => {
    const token = req.headers.authorization as string;
    const expiration = req.body.expiration;
    try {
        await this.userBusiness.changeTokenExpiration(
        token,
        expiration
      );
      res.status(200).send({ tokenexpiration });
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      }
      res.status(500).send("Erro ao alterar expiração do token");
    }
  }
}
