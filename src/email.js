const nodemailer = require('nodemailer');

let transport = nodemailer.createTransport({
    host: "smtp.office365.com",
    port: 587,
    secure: false,
    auth: {
        user: "metaleaguers@hotmail.com",
        pass: ""
    }
});


transport.sendMail({
    from: "Anderson Felix <metaleaguers@hotmail.com>",
    to: "aflyra@gmail.com",
    subject: "Testando envio de mensagens",
    text: "Prezado(a) Leaguers. \n\nVocê foi selecionado pra fazer a avaliação.\n\n",
    html: "Prezado(a) Leaguers. Você foi selecionado pra fazer a avaliação, segue o link do formulário <a href='https://www.meta.com.br'>nodemailer</a> boa sorte!"
}).then(message => {
    console.log(message);
}).catch(err => {
    console.log(err);
});


/*
** Para testar o envio de email, porem a aplicação não está funcionando na pasta do repositorio, teste na pasta local e funcionou.

Instalar o módulo para reiniciar o servidor sempre que houver alteração no código fonte, g significa globalmente
Executar no prompt de comando somente quando nunca utilizou o Nodemon
### npm install -g nodemon

Instalar o Nodemon no projeto
### npm install --save-dev nodemon

Módulo para enviar e-mail
### npm install nodemailer

Rodar o projeto com o Nodemon
### nodemon email.js

*/