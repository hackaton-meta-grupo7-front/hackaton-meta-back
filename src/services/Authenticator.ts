import * as jwt from "jsonwebtoken";
import { authenticationData } from "../types/authData";
import { guestAuthenticationData } from "../types/guestAuthData";

export let tokenexpiration = "48h";
export class Authenticator {
  generateToken = (payload: authenticationData): string => {
    return jwt.sign(payload, process.env.JWT_KEY as string, {
      expiresIn: process.env.JWT_EXPIRES_IN as string,
    });
  };

  getTokenData = (token: string): authenticationData => {
    return jwt.verify(
      token,
      process.env.JWT_KEY as string
    ) as authenticationData;
  };
  
  changeExpiration = (expiration: string): void => {
    tokenexpiration = expiration;
  };

  generateTokenForGuestAvaliator = (payload: guestAuthenticationData): string => {
    return jwt.sign(payload, process.env.JWT_KEY as string, {
      expiresIn: tokenexpiration as string,
    });
  };
}