import express from "express";

import UserController from "../controller/UserController";
import UserBusiness from "../business/UserBusiness";
import UserData from "../data/UserData";
import { Authenticator } from "../services/Authenticator";
import { HashManager } from "../services/HashManager";
import { IdGenerator } from "../services/IdGenerator";

export const userRouter = express.Router();

const userController = new UserController(
    new UserBusiness(
        new UserData(),
        new IdGenerator(),
        new HashManager(),
        new Authenticator()
    ),
);

userRouter.post("/signup", userController.signup);
userRouter.post("/login", userController.login);
userRouter.post("/createleaguer", userController.createLeaguer);
userRouter.get("/getleaguers", userController.getLeaguers);
userRouter.get("/leaguer/:leaguer_id", userController.getLeaguerById);
userRouter.put("/editleaguer/:leaguer_id", userController.editLeaguers);
userRouter.get("/showpayload", userController.showTokenPayload);
userRouter.put("/edituserrole", userController.editUserRole);
userRouter.put("/changetokenexpiration", userController.changeTokenExpiration);