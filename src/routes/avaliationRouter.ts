import express from "express";
import AvaliationController from "../controller/AvaliationController";
import AvaliationBusiness from "../business/AvaliationBusiness";
import AvaliationData from "../data/AvaliationDatabase";

export const avaliationRouter = express.Router();

const avaliationController = new AvaliationController(
    new AvaliationBusiness(
        new AvaliationData()
    )
);

avaliationRouter.get("/view/:avaliation_id", avaliationController.getAvaliation);
avaliationRouter.post("/createavaliation", avaliationController.createAvaliation);