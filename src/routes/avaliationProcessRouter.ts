import express from "express";

import AvaliationProcessController from "../controller/AvaliationProcessController";
import AvaliationProcessBusiness from "../business/AvaliationProcessBusiness";
import AvaliationProcessData from "../data/AvaliationProcessDatabase";
import { Authenticator } from "../services/Authenticator";
import { IdGenerator } from "../services/IdGenerator";
import UserData from "../data/UserData";

export const avaliationProcessRouter = express.Router();

const avaliationProcessController = new AvaliationProcessController(
        new AvaliationProcessBusiness(
        new UserData(),
        new AvaliationProcessData(),
        new IdGenerator(),
        new Authenticator()
    )
);

avaliationProcessRouter.post("/createavaliationprocess", avaliationProcessController.createAvaliationProcess);
avaliationProcessRouter.get("/viewavaliationprocesses", avaliationProcessController.viewAvaliationProcesses);
avaliationProcessRouter.get("/checkavaliationprocess", avaliationProcessController.checkIfAvaliationProcessExists);