import UserData from "../data/UserData";
import User from "../model/User";
import Leaguer from "../model/Leaguer";
import { Authenticator } from "../services/Authenticator";
import { HashManager } from "../services/HashManager";
import { IdGenerator } from "../services/IdGenerator";
import { SignupInputDTO } from "../types/signupInputDTO";
import { LoginInputDTO } from "../types/loginInputDTO";
import { LeaguerInputDTO } from "../types/leaguerInputDTO";
import { UserRole } from "../model/User";

export default class UserBusiness {
  constructor(
    private userData: UserData,
    private idGenerator: IdGenerator,
    private hashManager: HashManager,
    private authenticator: Authenticator
  ) {}

  signup = async (input: SignupInputDTO) => {
    //validacao
    const { name, username, email, password, role } = input;
    if (!email || !name || !password) {
      throw new Error("Campos inválidos");
    }

    //conferir se o usuario existe
    const registeredUser = await this.userData.findByEmail(email);
    if (registeredUser) {
      throw new Error("Email já cadastrado");
    }

    if (!email.includes("@")) {
      throw new Error("Email inválido");
    }

    //criar uma id pro usuario
    const id = this.idGenerator.generateId();

    //hashear o password
    const hashedPassword = await this.hashManager.hash(password);

    //criar o usuario no banco
    const user = new User(id, name, username, email, hashedPassword, role);
    await this.userData.insertUser(user);

    //criar o token
    const token = this.authenticator.generateToken({ id, role, name });

    //retornar o token
    return token;
  };

  login = async (input: LoginInputDTO) => {
    //validacao
    const { email, password } = input;
    if (!email || !password) {
      throw new Error("Campos inválidos");
    }

    //conferir se o usuario existe
    const user = await this.userData.findByEmail(email);
    if (!user) {
      throw new Error("Usuário não cadastrado");
    }

    //conferir se a senha está correta
    const isPasswordCorrect = await this.hashManager.compare(
      password,
      user.password
    );
    if (!isPasswordCorrect) {
      throw new Error("Senha incorreta");
    }

    //criar o token
    const token = this.authenticator.generateToken({
      id: user.id,
      role: user.role,
      name: user.name,
    });
    //retornar o token
    return token;
  };

  createLeaguer = async (input: LeaguerInputDTO, token: string) => {
    //validacao
    const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } =
      input;
    if (
      !leaguerEmail ||
      !leaguerName ||
      !leaguerClass ||
      !step ||
      !responsibleName
    ) {
      throw new Error("Campos inválidos");
    }

    if (!token) {
      throw new Error("Token inválido");
    }

    if (!leaguerEmail.includes("@")) {
      throw new Error("Email inválido");
    }

    //criar uma id pro usuario
    const leaguer_id = this.idGenerator.generateId();

    //conferir se o usuario existe
    const registeredLeaguer = await this.userData.findLeaguerById(leaguer_id);
    if (registeredLeaguer) {
      throw new Error("Leaguer já cadastrado");
    }

    const user = this.authenticator.getTokenData(token);

    if (user.role === "GESTOR") {
      throw new Error(
        "Acesso negado, só administradores e mentores podem criar leaguers"
      );
    }

    const creatorId = user.id;

    //criar o usuario no banco
    const leaguer = new Leaguer(
      responsibleName,
      leaguerName,
      leaguerEmail,
      leaguer_id,
      leaguerClass,
      step,
      creatorId
    );
    await this.userData.insertLeaguer(leaguer);

    //retornar o leaguer
    return leaguer;
  };

  getLeaguers = async (token: string) => {
    if (!token) {
      throw new Error("Token inválido");
    }

    const user = this.authenticator.getTokenData(token);

    if (user.role === "ADMIN" || user.role === "MENTOR") {
      const leaguers = await this.userData.getLeaguers();
      return leaguers;
    } else {
      const leaguers = await this.userData.getLeaguersByResponsibleName(
        user.name
      );
      return leaguers;
    }
  };

  getLeaguerById = async (id: string, token: string) => {
    if (!token) {
      throw new Error("Token inválido");
    }

    if (!id) {
      throw new Error("Id inválido");
    }

    const leaguer = await this.userData.findLeaguerById(id);
    const leaguerName = leaguer.leaguerName;

    const leaguerWithAvaliations =
      await this.userData.selectLeaguerWithAvaliations(id, leaguerName);
    return leaguerWithAvaliations;
  };

  editLeaguers = async (
    leaguer_id: string,
    input: LeaguerInputDTO,
    token: string
  ) => {
    //validacao
    const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } =
      input;

    if (!leaguer_id) {
      throw new Error("ID inválido");
    }
    if (!token) {
      throw new Error("Token inválido");
    }
    const user = this.authenticator.getTokenData(token);
    if (user.role === "ADMIN" || user.role === "MENTOR") {
      const leaguer = await this.userData.findLeaguerById(leaguer_id);
      if (!leaguer) {
        throw new Error("Leaguer não encontrado");
      }
      leaguer.responsibleName = responsibleName;
      leaguer.leaguerName = leaguerName;
      leaguer.leaguerEmail = leaguerEmail;
      leaguer.leaguerClass = leaguerClass;
      leaguer.step = step;
      await this.userData.updateLeaguer(leaguer_id, leaguer);
      return leaguer;
    } else {
      throw new Error(
        "Acesso negado, só administradores e mentores podem editar leaguers"
      );
    }
  };

  editUserRole = async (user_name: string, role: UserRole, token: string) => {
    if (!user_name) {
      throw new Error("Nome inválido");
    }
    if (!token) {
      throw new Error("Token inválido");
    }

    if (!role) {
      throw new Error("Role inválido");
    }

    const user = this.authenticator.getTokenData(token);
    if (user.role === "ADMIN") {
      const user = await this.userData.findByName(user_name);
      if (!user) {
        throw new Error("Usuário não encontrado");
      }

      await this.userData.updateUserRole(user.id, role);
      return user;
    } else {
      throw new Error(
        "Acesso negado, só administradores podem editar usuários"
      );
    }
  };

  showTokenPayload = async (token: string) => {
    if (!token) {
      throw new Error("Token inválido");
    }
    const user = this.authenticator.getTokenData(token);
    return user;
  };

  changeTokenExpiration = async (token: string, expiration: number) => {
    if (!token) {
      throw new Error("Token inválido");
    }
    if (!expiration) {
      throw new Error("Expiração inválida");
    }
    const tokenexpiration = expiration.toString()+"h";
    const user = this.authenticator.getTokenData(token);
    if (user.role === "ADMIN") {
      await this.authenticator.changeExpiration(tokenexpiration);
      return user;
    } else {
      throw new Error(
        "Acesso negado, só administradores podem alterar a expiração do token"
      );
    }
  }
}
