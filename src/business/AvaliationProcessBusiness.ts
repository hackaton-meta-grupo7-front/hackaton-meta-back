import AvaliationProcessData from "../data/AvaliationProcessDatabase";
import AvaliationProcess from "../model/AvaliationProcess";
import { AvaliationProcessInputDTO } from "../types/avaliationProcessInputDTO";
import { Authenticator } from "../services/Authenticator";
import { HashManager } from "../services/HashManager";
import { IdGenerator } from "../services/IdGenerator";
import { run as sendMail } from "../services/Mailer";
import UserData from "../data/UserData";

export default class AvaliationProcessBusiness {
  constructor(
    private userData: UserData,
    private avaliationData: AvaliationProcessData,
    private idGenerator: IdGenerator,
    private authenticator: Authenticator
  ) {}

  public async createAvaliationProcess(
    input: AvaliationProcessInputDTO,
    token: string
  ) {
    const { people_involved_emails, leaguer_id, colleagues } = input;
    const id = this.idGenerator.generateId();
    const user = this.authenticator.getTokenData(token);
    const responsible_id = user.id;
    const date = new Date();

    if (!people_involved_emails || !leaguer_id || !colleagues) {
      throw new Error("Campos inválidos");
    }

    if(!token){
      throw new Error("Token inválido");
    }

    function splitEmails(emails: string) {
      const emails_array = emails.split(",");
      return emails_array;
    }
    const emails_array = splitEmails(people_involved_emails);

    const emails_id = [];

    const guestRolePayload = "GUEST";

    const leaguer = await this.userData.findLeaguerById(leaguer_id);
    const leaguerName = leaguer.leaguerName;

    for (let i = 0; i < emails_array.length; i++) {
      const avaliation_id = this.idGenerator.generateId();
      emails_id.push(id);
      const guestToken = this.authenticator.generateTokenForGuestAvaliator(
        {
          avaliation_id: avaliation_id,
          guest_role: guestRolePayload,
          leaguer_name: leaguerName,
        });
        
      const avaliationLink = `http://metaleaguegrupo7.surge.sh/form/${guestToken}`;
      await sendMail(emails_array[i], avaliationLink);
    }

    const avaliationProcess = new AvaliationProcess(
      id,
      people_involved_emails,
      leaguer_id,
      responsible_id,
      colleagues,
      date,
      emails_id.toString()
    );

    await this.avaliationData.insert(avaliationProcess);

    return avaliationProcess;
  }

  public async viewAvaliationProcesses(token: string, leaguerId: string) {
    const user = this.authenticator.getTokenData(token);

    if(!leaguerId){
      throw new Error("Campos inválidos");
    }
    if(!token){
      throw new Error("Token inválido");
    }
    const avaliations = await this.avaliationData.selectAvaliationProcess(
      user.id,
      leaguerId
    );
    return avaliations;
  }

  public async checkIfAvaliationProcessExists(
    token: string,
    leaguerId: string
  ) {
    if(!leaguerId){
      throw new Error("Campos inválidos");
    }
    if(!token){
      throw new Error("Token inválido");
    }
    const user = this.authenticator.getTokenData(token);
    const leaguer_id = leaguerId;
    const avaliations =
      await this.avaliationData.checkIfAvaliationProcessExists(
        user.id,
        leaguer_id
      );
    return avaliations;
  }
}
