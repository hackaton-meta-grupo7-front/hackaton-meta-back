export default class AvaliationProcess {
    constructor(
        public id: string,
        public people_involved_emails: string,
        public leaguer_id: string,
        public responsible_id: string,
        public colleagues: string,
        public date: Date,
        public avaliations: string
    ) {}
}