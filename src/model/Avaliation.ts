
export default class Avaliation {
    constructor(
        
        public avaliation_id: string,
        public avaliation_by: string,
        public projects : any,
        public technology : any,
        public created_at: Date,
        public time_in_program: string,
        public professional_name: string,
        public performance_unique: string,
        public performance_comment: string,
        public delivered_unique: string,
        public delivered_comment: string,
        public proativity_unique: string,
        public proativity_comment: string,
        public commitment_unique: string,
        public commitment_comment: string,
        public teamplayer_unique: string,
        public teamplayer_comment: string,
        public developedskills_unique: string,
        public developedskills_comment: string,
        public leardshipskills_unique: string,
        public leardshipskills_comment: string,
        public pontuality_unique: string,
        public pontuality_comment: string,
        public workingunderpression_unique: string,
        public workingunderpression_comment: string,
        public engagementleasonlearned_unique: string,
        public engagementleasonlearned_comment: string,
        public administrative_activities_unique: string,
        public administrative_activities_comment: string,
        public professional_caracteristics_highlights: string,
        public general_considerations: string
        
    ) {}
}