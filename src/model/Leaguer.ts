export default class Leaguer {
    constructor(
    public responsibleName: string,
    public leaguerName: string,
    public leaguerEmail: string,
    public leaguer_id: string,
    public leaguerClass: LeaguerClass,
    public step: LeaguerStep,
    public creatorId: string
    ) {}
}
             
export enum LeaguerClass {
    TURMA_PILOTO = "TURMA_PILOTO",
    TURMA_1 = "TURMA_1",
    TURMA_2 = "TURMA_2",
    TURMA_3 = "TURMA_3"
}

export enum LeaguerStep {
    INTRODUCAO = "INTRODUCAO",
    LABS = "LABS",
    BETA = "BETA"
}