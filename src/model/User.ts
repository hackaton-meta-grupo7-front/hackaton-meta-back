export default class User{
    constructor(
        private id:string,
        private name:string,
        private username: string,
        private email:string,
        private password :string,
        private role:UserRole
    ){}
}

export enum UserRole{
    ADMIN = "ADMIN",
    MENTOR = "MENTOR",
    GESTOR = "GESTOR"
}

