"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class AvaliationController {
    constructor(avaliationBusiness) {
        this.avaliationBusiness = avaliationBusiness;
        this.createAvaliation = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const avaliation_id = req.body.avaliation_id;
                const input = req.body;
                const avaliation = yield this.avaliationBusiness.createAvaliationForm(avaliation_id, input, token);
                res.status(200).send({ avaliation });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao criar avaliação");
            }
        });
        this.getAvaliation = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const avaliation_id = req.params.avaliation_id;
                const result = yield this.avaliationBusiness.getAvaliationById(avaliation_id, token);
                res.status(200).send({ result });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao buscar avaliação");
            }
        });
    }
}
exports.default = AvaliationController;
//# sourceMappingURL=AvaliationController.js.map