"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Authenticator_1 = require("../services/Authenticator");
class UserController {
    constructor(userBusiness) {
        this.userBusiness = userBusiness;
        this.signup = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { name, username, email, password, role } = req.body;
            const input = {
                name,
                username,
                email,
                password,
                role,
            };
            try {
                const token = yield this.userBusiness.signup(input);
                res
                    .status(201)
                    .send({ message: "Usuário cadastrado com sucesso", token });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro no signup");
            }
        });
        this.login = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { email, password } = req.body;
            const input = {
                email,
                password,
            };
            try {
                const token = yield this.userBusiness.login(input);
                res.status(200).send({ message: "Usuário logado com sucesso", token });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro no login");
            }
        });
        this.createLeaguer = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const token = req.headers.authorization;
            const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } = req.body;
            const input = {
                responsibleName,
                leaguerName,
                leaguerEmail,
                leaguerClass,
                step,
            };
            try {
                yield this.userBusiness.createLeaguer(input, token);
                res.status(201).send({ message: "Leaguer criado com sucesso!" });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao criar Leaguer");
            }
        });
        this.editLeaguers = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const token = req.headers.authorization;
            const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } = req.body;
            const leaguer_id = req.params.leaguer_id;
            const input = {
                responsibleName,
                leaguerName,
                leaguerEmail,
                leaguerClass,
                step,
            };
            try {
                yield this.userBusiness.editLeaguers(leaguer_id, input, token);
                res.status(201).send({ message: "Leaguer editado com sucesso!" });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao editar Leaguer");
            }
        });
        this.editUserRole = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const token = req.headers.authorization;
            const role = req.body.role;
            const user_name = req.body.user_name;
            try {
                yield this.userBusiness.editUserRole(user_name, role, token);
                res.status(201).send({ message: "Role editado com sucesso!" });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao editar Role");
            }
        });
        this.getLeaguers = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const leaguers = yield this.userBusiness.getLeaguers(token);
                res.status(200).send({ leaguers });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao buscar Leaguers");
            }
        });
        this.getLeaguerById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const token = req.headers.authorization;
            const leaguer_id = req.params.leaguer_id;
            try {
                const leaguer = yield this.userBusiness.getLeaguerById(leaguer_id, token);
                res.status(200).send({ leaguer });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao buscar Leaguer");
            }
        });
        this.showTokenPayload = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const result = yield this.userBusiness.showTokenPayload(token);
                res.status(200).send({ result });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao mostrar payload do token");
            }
        });
        this.changeTokenExpiration = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const token = req.headers.authorization;
            const expiration = req.body.expiration;
            try {
                yield this.userBusiness.changeTokenExpiration(token, expiration);
                res.status(200).send({ tokenexpiration: Authenticator_1.tokenexpiration });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao alterar expiração do token");
            }
        });
    }
}
exports.default = UserController;
//# sourceMappingURL=UserController.js.map