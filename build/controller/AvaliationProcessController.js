"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class AvaliationProcessController {
    constructor(avaliationProcessBusiness) {
        this.avaliationProcessBusiness = avaliationProcessBusiness;
        this.createAvaliationProcess = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const input = req.body;
                const process = yield this.avaliationProcessBusiness.createAvaliationProcess(input, token);
                res.status(200).send({ process });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao criar processo de avaliação");
            }
        });
        this.viewAvaliationProcesses = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const leaguerId = req.body.leaguerId;
                const avaliation_process = yield this.avaliationProcessBusiness.viewAvaliationProcesses(token, leaguerId);
                res.status(200).send({ avaliation_process });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao visualizar avaliações");
            }
        });
        this.checkIfAvaliationProcessExists = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const token = req.headers.authorization;
                const leaguerId = req.body.leaguerId;
                const result = yield this.avaliationProcessBusiness.checkIfAvaliationProcessExists(token, leaguerId);
                res.status(200).send({ result });
            }
            catch (error) {
                if (error instanceof Error) {
                    return res.status(400).send(error.message);
                }
                res.status(500).send("Erro ao verificar se existe processo de avaliação");
            }
        });
    }
}
exports.default = AvaliationProcessController;
//# sourceMappingURL=AvaliationProcessController.js.map