"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Avaliation_1 = __importDefault(require("../model/Avaliation"));
class AvaliationBusiness {
    constructor(avaliationData) {
        this.avaliationData = avaliationData;
    }
    createAvaliationForm(avaliationId, input, token) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                throw new Error("Invalid token");
            }
            if (!avaliationId) {
                throw new Error("Invalid avaliation id");
            }
            const avaliationAlreadyExists = yield this.avaliationData.avaliationExists(avaliationId);
            if (avaliationAlreadyExists) {
                throw new Error("Avaliation with this ID already exists");
            }
            const avaliation_id = avaliationId;
            const created_at = new Date();
            const avaliation_by = input.avaliation_by;
            const projects = input.projects;
            const technologies = input.technologies;
            const time_in_program = input.time_in_program;
            const professional_name = input.professional_name;
            const performance_unique = input.performance_unique;
            const performance_comment = input.performance_comment;
            const delivered_unique = input.delivered_unique;
            const delivered_comment = input.delivered_comment;
            const proativity_unique = input.proativity_unique;
            const proativity_comment = input.proativity_comment;
            const commitment_unique = input.commitment_unique;
            const commitment_comment = input.commitment_comment;
            const teamplayer_unique = input.teamplayer_unique;
            const teamplayer_comment = input.teamplayer_comment;
            const developedskills_unique = input.developedskills_unique;
            const developedskills_comment = input.developedskills_comment;
            const leardshipskills_unique = input.leardshipskills_unique;
            const leardshipskills_comment = input.leardshipskills_comment;
            const pontuality_unique = input.pontuality_unique;
            const pontuality_comment = input.pontuality_comment;
            const workingunderpression_unique = input.workingunderpression_unique;
            const workingunderpression_comment = input.workingunderpression_comment;
            const engagementleasonlearned_unique = input.engagementleasonlearned_unique;
            const engagementleasonlearned_comment = input.engagementleasonlearned_comment;
            const administrative_activities_unique = input.administrative_activities_unique;
            const administrative_activities_comment = input.administrative_activities_comment;
            const professional_caracteristics_highlights = input.professional_caracteristics_highlights;
            const general_considerations = input.general_considerations;
            if (projects[0] === 1) {
                projects[0] = "META_PEOPLE";
            }
            if (projects[0] === 2) {
                projects[0] = "META_SKILLS";
            }
            if (projects[1] === 1) {
                projects[1] = "META_PEOPLE";
            }
            if (projects[1] === 2) {
                projects[1] = "META_SKILLS";
            }
            const avaliation = new Avaliation_1.default(avaliation_id, avaliation_by, projects.toString(), technologies.toString(), created_at, time_in_program, professional_name, performance_unique, performance_comment, delivered_unique, delivered_comment, proativity_unique, proativity_comment, commitment_unique, commitment_comment, teamplayer_unique, teamplayer_comment, developedskills_unique, developedskills_comment, leardshipskills_unique, leardshipskills_comment, pontuality_unique, pontuality_comment, workingunderpression_unique, workingunderpression_comment, engagementleasonlearned_unique, engagementleasonlearned_comment, administrative_activities_unique, administrative_activities_comment, professional_caracteristics_highlights, general_considerations);
            if (!avaliation_id ||
                !avaliation_by ||
                !projects ||
                !technologies ||
                !created_at ||
                !time_in_program ||
                !professional_name ||
                !performance_unique ||
                !performance_comment ||
                !delivered_unique ||
                !delivered_comment ||
                !proativity_unique ||
                !proativity_comment ||
                !commitment_unique ||
                !commitment_comment ||
                !teamplayer_unique ||
                !teamplayer_comment ||
                !developedskills_unique ||
                !developedskills_comment ||
                !leardshipskills_unique ||
                !leardshipskills_comment ||
                !pontuality_unique ||
                !pontuality_comment ||
                !workingunderpression_unique ||
                !workingunderpression_comment ||
                !engagementleasonlearned_unique ||
                !engagementleasonlearned_comment ||
                !administrative_activities_unique ||
                !administrative_activities_comment ||
                !professional_caracteristics_highlights ||
                !general_considerations) {
                throw new Error("Invalid input");
            }
            const avaliationData = yield this.avaliationData.insertAvaliation(avaliation);
            return avaliationData;
        });
    }
    getAvaliationById(avaliationId, token) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                throw new Error("Invalid token");
            }
            if (!avaliationId) {
                throw new Error("Invalid avaliation id");
            }
            const avaliation = yield this.avaliationData.selectAvaliationById(avaliationId);
            return avaliation;
        });
    }
}
exports.default = AvaliationBusiness;
//# sourceMappingURL=AvaliationBusiness.js.map