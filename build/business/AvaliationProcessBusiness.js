"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const AvaliationProcess_1 = __importDefault(require("../model/AvaliationProcess"));
const Mailer_1 = require("../services/Mailer");
class AvaliationProcessBusiness {
    constructor(userData, avaliationData, idGenerator, authenticator) {
        this.userData = userData;
        this.avaliationData = avaliationData;
        this.idGenerator = idGenerator;
        this.authenticator = authenticator;
    }
    createAvaliationProcess(input, token) {
        return __awaiter(this, void 0, void 0, function* () {
            const { people_involved_emails, leaguer_id, colleagues } = input;
            const id = this.idGenerator.generateId();
            const user = this.authenticator.getTokenData(token);
            const responsible_id = user.id;
            const date = new Date();
            if (!people_involved_emails || !leaguer_id || !colleagues) {
                throw new Error("Campos inválidos");
            }
            if (!token) {
                throw new Error("Token inválido");
            }
            function splitEmails(emails) {
                const emails_array = emails.split(",");
                return emails_array;
            }
            const emails_array = splitEmails(people_involved_emails);
            const emails_id = [];
            const guestRolePayload = "GUEST";
            const leaguer = yield this.userData.findLeaguerById(leaguer_id);
            const leaguerName = leaguer.leaguerName;
            for (let i = 0; i < emails_array.length; i++) {
                const avaliation_id = this.idGenerator.generateId();
                emails_id.push(id);
                const guestToken = this.authenticator.generateTokenForGuestAvaliator({
                    avaliation_id: avaliation_id,
                    guest_role: guestRolePayload,
                    leaguer_name: leaguerName,
                });
                const avaliationLink = `http://localhost:3000/form/${guestToken}`;
                yield (0, Mailer_1.run)(emails_array[i], avaliationLink);
            }
            const avaliationProcess = new AvaliationProcess_1.default(id, people_involved_emails, leaguer_id, responsible_id, colleagues, date, emails_id.toString());
            yield this.avaliationData.insert(avaliationProcess);
            return avaliationProcess;
        });
    }
    viewAvaliationProcesses(token, leaguerId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = this.authenticator.getTokenData(token);
            if (!leaguerId) {
                throw new Error("Campos inválidos");
            }
            if (!token) {
                throw new Error("Token inválido");
            }
            const avaliations = yield this.avaliationData.selectAvaliationProcess(user.id, leaguerId);
            return avaliations;
        });
    }
    checkIfAvaliationProcessExists(token, leaguerId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!leaguerId) {
                throw new Error("Campos inválidos");
            }
            if (!token) {
                throw new Error("Token inválido");
            }
            const user = this.authenticator.getTokenData(token);
            const leaguer_id = leaguerId;
            const avaliations = yield this.avaliationData.checkIfAvaliationProcessExists(user.id, leaguer_id);
            return avaliations;
        });
    }
}
exports.default = AvaliationProcessBusiness;
//# sourceMappingURL=AvaliationProcessBusiness.js.map