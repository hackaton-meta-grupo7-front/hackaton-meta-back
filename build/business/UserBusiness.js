"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = __importDefault(require("../model/User"));
const Leaguer_1 = __importDefault(require("../model/Leaguer"));
class UserBusiness {
    constructor(userData, idGenerator, hashManager, authenticator) {
        this.userData = userData;
        this.idGenerator = idGenerator;
        this.hashManager = hashManager;
        this.authenticator = authenticator;
        this.signup = (input) => __awaiter(this, void 0, void 0, function* () {
            const { name, username, email, password, role } = input;
            if (!email || !name || !password) {
                throw new Error("Campos inválidos");
            }
            const registeredUser = yield this.userData.findByEmail(email);
            if (registeredUser) {
                throw new Error("Email já cadastrado");
            }
            if (!email.includes("@")) {
                throw new Error("Email inválido");
            }
            const id = this.idGenerator.generateId();
            const hashedPassword = yield this.hashManager.hash(password);
            const user = new User_1.default(id, name, username, email, hashedPassword, role);
            yield this.userData.insertUser(user);
            const token = this.authenticator.generateToken({ id, role, name });
            return token;
        });
        this.login = (input) => __awaiter(this, void 0, void 0, function* () {
            const { email, password } = input;
            if (!email || !password) {
                throw new Error("Campos inválidos");
            }
            const user = yield this.userData.findByEmail(email);
            if (!user) {
                throw new Error("Usuário não cadastrado");
            }
            const isPasswordCorrect = yield this.hashManager.compare(password, user.password);
            if (!isPasswordCorrect) {
                throw new Error("Senha incorreta");
            }
            const token = this.authenticator.generateToken({
                id: user.id,
                role: user.role,
                name: user.name,
            });
            return token;
        });
        this.createLeaguer = (input, token) => __awaiter(this, void 0, void 0, function* () {
            const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } = input;
            if (!leaguerEmail ||
                !leaguerName ||
                !leaguerClass ||
                !step ||
                !responsibleName) {
                throw new Error("Campos inválidos");
            }
            if (!token) {
                throw new Error("Token inválido");
            }
            if (!leaguerEmail.includes("@")) {
                throw new Error("Email inválido");
            }
            const leaguer_id = this.idGenerator.generateId();
            const registeredLeaguer = yield this.userData.findLeaguerById(leaguer_id);
            if (registeredLeaguer) {
                throw new Error("Leaguer já cadastrado");
            }
            const user = this.authenticator.getTokenData(token);
            if (user.role === "GESTOR") {
                throw new Error("Acesso negado, só administradores e mentores podem criar leaguers");
            }
            const creatorId = user.id;
            const leaguer = new Leaguer_1.default(responsibleName, leaguerName, leaguerEmail, leaguer_id, leaguerClass, step, creatorId);
            yield this.userData.insertLeaguer(leaguer);
            return leaguer;
        });
        this.getLeaguers = (token) => __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                throw new Error("Token inválido");
            }
            const user = this.authenticator.getTokenData(token);
            if (user.role === "ADMIN" || user.role === "MENTOR") {
                const leaguers = yield this.userData.getLeaguers();
                return leaguers;
            }
            else {
                const leaguers = yield this.userData.getLeaguersByResponsibleName(user.name);
                return leaguers;
            }
        });
        this.getLeaguerById = (id, token) => __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                throw new Error("Token inválido");
            }
            if (!id) {
                throw new Error("Id inválido");
            }
            const leaguer = yield this.userData.findLeaguerById(id);
            const leaguerName = leaguer.leaguerName;
            const leaguerWithAvaliations = yield this.userData.selectLeaguerWithAvaliations(id, leaguerName);
            return leaguerWithAvaliations;
        });
        this.editLeaguers = (leaguer_id, input, token) => __awaiter(this, void 0, void 0, function* () {
            const { responsibleName, leaguerName, leaguerEmail, leaguerClass, step } = input;
            if (!leaguer_id) {
                throw new Error("ID inválido");
            }
            if (!token) {
                throw new Error("Token inválido");
            }
            const user = this.authenticator.getTokenData(token);
            if (user.role === "ADMIN" || user.role === "MENTOR") {
                const leaguer = yield this.userData.findLeaguerById(leaguer_id);
                if (!leaguer) {
                    throw new Error("Leaguer não encontrado");
                }
                leaguer.responsibleName = responsibleName;
                leaguer.leaguerName = leaguerName;
                leaguer.leaguerEmail = leaguerEmail;
                leaguer.leaguerClass = leaguerClass;
                leaguer.step = step;
                yield this.userData.updateLeaguer(leaguer_id, leaguer);
                return leaguer;
            }
            else {
                throw new Error("Acesso negado, só administradores e mentores podem editar leaguers");
            }
        });
        this.editUserRole = (user_name, role, token) => __awaiter(this, void 0, void 0, function* () {
            if (!user_name) {
                throw new Error("Nome inválido");
            }
            if (!token) {
                throw new Error("Token inválido");
            }
            if (!role) {
                throw new Error("Role inválido");
            }
            const user = this.authenticator.getTokenData(token);
            if (user.role === "ADMIN") {
                const user = yield this.userData.findByName(user_name);
                if (!user) {
                    throw new Error("Usuário não encontrado");
                }
                yield this.userData.updateUserRole(user.id, role);
                return user;
            }
            else {
                throw new Error("Acesso negado, só administradores podem editar usuários");
            }
        });
        this.showTokenPayload = (token) => __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                throw new Error("Token inválido");
            }
            const user = this.authenticator.getTokenData(token);
            return user;
        });
        this.changeTokenExpiration = (token, expiration) => __awaiter(this, void 0, void 0, function* () {
            if (!token) {
                throw new Error("Token inválido");
            }
            if (!expiration) {
                throw new Error("Expiração inválida");
            }
            const tokenexpiration = expiration.toString() + "h";
            const user = this.authenticator.getTokenData(token);
            if (user.role === "ADMIN") {
                yield this.authenticator.changeExpiration(tokenexpiration);
                return user;
            }
            else {
                throw new Error("Acesso negado, só administradores podem alterar a expiração do token");
            }
        });
    }
}
exports.default = UserBusiness;
//# sourceMappingURL=UserBusiness.js.map