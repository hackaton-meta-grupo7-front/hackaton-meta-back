"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AvaliationProcess {
    constructor(id, people_involved_emails, leaguer_id, responsible_id, colleagues, date, avaliations) {
        this.id = id;
        this.people_involved_emails = people_involved_emails;
        this.leaguer_id = leaguer_id;
        this.responsible_id = responsible_id;
        this.colleagues = colleagues;
        this.date = date;
        this.avaliations = avaliations;
    }
}
exports.default = AvaliationProcess;
//# sourceMappingURL=AvaliationProcess.js.map