"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Avaliation {
    constructor(avaliation_id, avaliation_by, projects, technology, created_at, time_in_program, professional_name, performance_unique, performance_comment, delivered_unique, delivered_comment, proativity_unique, proativity_comment, commitment_unique, commitment_comment, teamplayer_unique, teamplayer_comment, developedskills_unique, developedskills_comment, leardshipskills_unique, leardshipskills_comment, pontuality_unique, pontuality_comment, workingunderpression_unique, workingunderpression_comment, engagementleasonlearned_unique, engagementleasonlearned_comment, administrative_activities_unique, administrative_activities_comment, professional_caracteristics_highlights, general_considerations) {
        this.avaliation_id = avaliation_id;
        this.avaliation_by = avaliation_by;
        this.projects = projects;
        this.technology = technology;
        this.created_at = created_at;
        this.time_in_program = time_in_program;
        this.professional_name = professional_name;
        this.performance_unique = performance_unique;
        this.performance_comment = performance_comment;
        this.delivered_unique = delivered_unique;
        this.delivered_comment = delivered_comment;
        this.proativity_unique = proativity_unique;
        this.proativity_comment = proativity_comment;
        this.commitment_unique = commitment_unique;
        this.commitment_comment = commitment_comment;
        this.teamplayer_unique = teamplayer_unique;
        this.teamplayer_comment = teamplayer_comment;
        this.developedskills_unique = developedskills_unique;
        this.developedskills_comment = developedskills_comment;
        this.leardshipskills_unique = leardshipskills_unique;
        this.leardshipskills_comment = leardshipskills_comment;
        this.pontuality_unique = pontuality_unique;
        this.pontuality_comment = pontuality_comment;
        this.workingunderpression_unique = workingunderpression_unique;
        this.workingunderpression_comment = workingunderpression_comment;
        this.engagementleasonlearned_unique = engagementleasonlearned_unique;
        this.engagementleasonlearned_comment = engagementleasonlearned_comment;
        this.administrative_activities_unique = administrative_activities_unique;
        this.administrative_activities_comment = administrative_activities_comment;
        this.professional_caracteristics_highlights = professional_caracteristics_highlights;
        this.general_considerations = general_considerations;
    }
}
exports.default = Avaliation;
//# sourceMappingURL=Avaliation.js.map