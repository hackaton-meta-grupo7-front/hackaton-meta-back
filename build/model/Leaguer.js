"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LeaguerStep = exports.LeaguerClass = void 0;
class Leaguer {
    constructor(responsibleName, leaguerName, leaguerEmail, leaguer_id, leaguerClass, step, creatorId) {
        this.responsibleName = responsibleName;
        this.leaguerName = leaguerName;
        this.leaguerEmail = leaguerEmail;
        this.leaguer_id = leaguer_id;
        this.leaguerClass = leaguerClass;
        this.step = step;
        this.creatorId = creatorId;
    }
}
exports.default = Leaguer;
var LeaguerClass;
(function (LeaguerClass) {
    LeaguerClass["TURMA_PILOTO"] = "TURMA_PILOTO";
    LeaguerClass["TURMA_1"] = "TURMA_1";
    LeaguerClass["TURMA_2"] = "TURMA_2";
    LeaguerClass["TURMA_3"] = "TURMA_3";
})(LeaguerClass = exports.LeaguerClass || (exports.LeaguerClass = {}));
var LeaguerStep;
(function (LeaguerStep) {
    LeaguerStep["INTRODUCAO"] = "INTRODUCAO";
    LeaguerStep["LABS"] = "LABS";
    LeaguerStep["BETA"] = "BETA";
})(LeaguerStep = exports.LeaguerStep || (exports.LeaguerStep = {}));
//# sourceMappingURL=Leaguer.js.map