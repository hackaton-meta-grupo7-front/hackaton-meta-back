"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const BaseDatabase_1 = require("./BaseDatabase");
class UserData extends BaseDatabase_1.BaseDatabase {
    constructor() {
        super(...arguments);
        this.TABLE_NAME = "meta_users_id";
        this.LEAGUER_TABLE_NAME = "meta_leaguers_profile";
        this.AVALIATIONS_TABLE_NAME = "meta_leaguers_avaliations";
        this.insertUser = (user) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.connection(this.TABLE_NAME).insert(user);
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.findByEmail = (email) => __awaiter(this, void 0, void 0, function* () {
            try {
                const queryResult = yield this.connection(this.TABLE_NAME)
                    .select()
                    .where({ email });
                return queryResult[0];
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.findByName = (name) => __awaiter(this, void 0, void 0, function* () {
            try {
                const queryResult = yield this.connection(this.TABLE_NAME)
                    .select()
                    .where({ name });
                return queryResult[0];
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.updateUserRole = (id, role) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.connection(this.TABLE_NAME)
                    .update({ role })
                    .where({ id });
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.insertLeaguer = (leaguer) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.connection(this.LEAGUER_TABLE_NAME).insert(leaguer);
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.findLeaguerById = (leaguer_id) => __awaiter(this, void 0, void 0, function* () {
            try {
                const queryResult = yield this.connection(this.LEAGUER_TABLE_NAME)
                    .select()
                    .where({ leaguer_id });
                return queryResult[0];
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.getLeaguers = () => __awaiter(this, void 0, void 0, function* () {
            try {
                return this.connection(this.LEAGUER_TABLE_NAME).select("*");
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.getLeaguersByResponsibleName = (responsibleName) => __awaiter(this, void 0, void 0, function* () {
            try {
                return this.connection(this.LEAGUER_TABLE_NAME)
                    .select("*")
                    .where({ responsibleName });
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.selectLeaguerWithAvaliations = (leaguer_id, professional_name) => __awaiter(this, void 0, void 0, function* () {
            try {
                const queryResult = yield this.connection(this.LEAGUER_TABLE_NAME)
                    .select("*")
                    .where({ leaguer_id });
                const leaguer = queryResult[0];
                const queryResultAvaliations = yield this.connection(this.AVALIATIONS_TABLE_NAME)
                    .select("*")
                    .where({ professional_name })
                    .orderBy("created_at", "desc");
                leaguer.avaliation = queryResultAvaliations;
                return leaguer;
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.updateLeaguer = (leaguer_id, input) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.connection(this.LEAGUER_TABLE_NAME)
                    .update(input)
                    .where({ leaguer_id });
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
    }
}
exports.default = UserData;
//# sourceMappingURL=UserData.js.map