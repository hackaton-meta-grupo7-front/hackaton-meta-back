"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const BaseDatabase_1 = require("./BaseDatabase");
class AvaliationData extends BaseDatabase_1.BaseDatabase {
    constructor() {
        super(...arguments);
        this.AVALIATION_TABLE_NAME = "meta_leaguers_avaliations";
        this.insertAvaliation = (avaliation) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.connection(this.AVALIATION_TABLE_NAME).insert(avaliation);
                return avaliation;
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.selectAvaliationById = (avaliation_id) => __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this.connection(this.AVALIATION_TABLE_NAME)
                    .select("*")
                    .where({ avaliation_id });
                return {
                    avaliation: result[0],
                };
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.avaliationExists = (avaliation_id) => __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this.connection(this.AVALIATION_TABLE_NAME)
                    .select("*")
                    .where({ avaliation_id });
                return result.length > 0;
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
        this.selectAvaliationsByLeaguerName = (professional_name) => __awaiter(this, void 0, void 0, function* () {
            try {
                const avaliations = yield this.connection(this.AVALIATION_TABLE_NAME)
                    .select("*")
                    .where({ professional_name });
                return avaliations;
            }
            catch (error) {
                if (error instanceof Error) {
                    throw new Error(error.message);
                }
                else {
                    throw new Error("Erro do banco !");
                }
            }
        });
    }
}
exports.default = AvaliationData;
//# sourceMappingURL=AvaliationDatabase.js.map