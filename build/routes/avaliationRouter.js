"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.avaliationRouter = void 0;
const express_1 = __importDefault(require("express"));
const AvaliationController_1 = __importDefault(require("../controller/AvaliationController"));
const AvaliationBusiness_1 = __importDefault(require("../business/AvaliationBusiness"));
const AvaliationDatabase_1 = __importDefault(require("../data/AvaliationDatabase"));
exports.avaliationRouter = express_1.default.Router();
const avaliationController = new AvaliationController_1.default(new AvaliationBusiness_1.default(new AvaliationDatabase_1.default()));
exports.avaliationRouter.get("/view/:avaliation_id", avaliationController.getAvaliation);
exports.avaliationRouter.post("/createavaliation", avaliationController.createAvaliation);
//# sourceMappingURL=avaliationRouter.js.map