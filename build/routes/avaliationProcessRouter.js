"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.avaliationProcessRouter = void 0;
const express_1 = __importDefault(require("express"));
const AvaliationProcessController_1 = __importDefault(require("../controller/AvaliationProcessController"));
const AvaliationProcessBusiness_1 = __importDefault(require("../business/AvaliationProcessBusiness"));
const AvaliationProcessDatabase_1 = __importDefault(require("../data/AvaliationProcessDatabase"));
const Authenticator_1 = require("../services/Authenticator");
const IdGenerator_1 = require("../services/IdGenerator");
const UserData_1 = __importDefault(require("../data/UserData"));
exports.avaliationProcessRouter = express_1.default.Router();
const avaliationProcessController = new AvaliationProcessController_1.default(new AvaliationProcessBusiness_1.default(new UserData_1.default(), new AvaliationProcessDatabase_1.default(), new IdGenerator_1.IdGenerator(), new Authenticator_1.Authenticator()));
exports.avaliationProcessRouter.post("/createavaliationprocess", avaliationProcessController.createAvaliationProcess);
exports.avaliationProcessRouter.get("/viewavaliationprocesses", avaliationProcessController.viewAvaliationProcesses);
exports.avaliationProcessRouter.get("/checkavaliationprocess", avaliationProcessController.checkIfAvaliationProcessExists);
//# sourceMappingURL=avaliationProcessRouter.js.map