"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRouter = void 0;
const express_1 = __importDefault(require("express"));
const UserController_1 = __importDefault(require("../controller/UserController"));
const UserBusiness_1 = __importDefault(require("../business/UserBusiness"));
const UserData_1 = __importDefault(require("../data/UserData"));
const Authenticator_1 = require("../services/Authenticator");
const HashManager_1 = require("../services/HashManager");
const IdGenerator_1 = require("../services/IdGenerator");
exports.userRouter = express_1.default.Router();
const userController = new UserController_1.default(new UserBusiness_1.default(new UserData_1.default(), new IdGenerator_1.IdGenerator(), new HashManager_1.HashManager(), new Authenticator_1.Authenticator()));
exports.userRouter.post("/signup", userController.signup);
exports.userRouter.post("/login", userController.login);
exports.userRouter.post("/createleaguer", userController.createLeaguer);
exports.userRouter.get("/getleaguers", userController.getLeaguers);
exports.userRouter.get("/leaguer/:leaguer_id", userController.getLeaguerById);
exports.userRouter.put("/editleaguer/:leaguer_id", userController.editLeaguers);
exports.userRouter.get("/showpayload", userController.showTokenPayload);
exports.userRouter.put("/edituserrole", userController.editUserRole);
exports.userRouter.put("/changetokenexpiration", userController.changeTokenExpiration);
//# sourceMappingURL=userRouter.js.map