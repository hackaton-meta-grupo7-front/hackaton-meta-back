# DESAFIO META

## Meta League

O Meta League é um programa que visa a atração de potenciais profissionais através de parcerias com escolas técnicas, universidades e
Edtechs, o nosso objetivo é acelerar o desenvolvimento dos profissionais para que estejam prontos para projetos de transformação digital com as
competências necessárias para atender as demandas do negócio.


### Caracteristica do Projeto

Aplicação web de ponta a ponta, feita como parte do Hackaton Meta. O propósito da aplicação é a criação de um sistema de avaliações onde o administrador possa criar e editar usuários e leaguers, esses usuários são capazes de criar leaguers, criar avaliações para esses leaguers e acompanhá-las. Todos os dados persistem em banco de dados.


## Funcionalidades

O projeto consiste em realizar as seguintes funcionalidades:

• Automação e informatização do processo de levantamento de indicadores quentes;
• Remoção de planilhas e forms;
• Centralização e consolidação dos dados de feedback;
• Manter histórico e acompanhamento dos profissionais.
• Controle de perfis;
• Envio de e-mail;
• Autenticação;
• Desenvolvimento de formulários;
• Usabilidade e experiência do usuário.

## DEMO
http://metaleaguegrupo7.surge.sh

## layout

Formato Web


![LoginM.png](./LoginM.png)                             ![Criar LeaguersM.png](./Criar LeaguersM.png)





# Como executar o projeto
Este projeto é divido em duas partes:

Backend (pasta server)
Frontend (pasta web)

💡No Frontend precisa que o Backend esteja sendo executado para funcionar.

### Pré-requisitos
Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas: GitLab, Node.js. Além disto é bom ter um editor para trabalhar com o código como VSCode

🎲 Rodando o Backend (servidor)
```
Clone este repositório
$ git clone https://gitlab.com/hackaton-meta-grupo7-front/hackaton-meta-back.git

Instale as dependências
$ npm install

Execute a aplicação em modo de desenvolvimento
$ npm run dev

-  O servidor inciará na porta:3003 - acesse http://localhost:3003 
```

🧭 Rodando a aplicação web (Frontend)
```
Clone este repositório
$ git clone https://gitlab.com/hackaton-meta-grupo7-front/hackaton-meta-front.git

Instale as dependências
$ npm install

Execute a aplicação em modo de desenvolvimento
$ npm run dev

-  O servidor inciará na porta:3000 - acesse http://localhost:3000 

```

### LINK DA DOCUMENTAÇÃO NO POSTMAN:

https://documenter.getpostman.com/view/19297915/Uz5AtKqA

## PARA RODAR A APLICAÇÃO

* npm install
* npm run dev

### 🛠 Tecnologias
As seguintes ferramentas foram usadas na construção do projeto:

- Website (React + TypeScript)
- React Router Dom
- Mui
- Axios
- CSS
- uuid

Veja o arquivo package.json

- Server (NodeJS + TypeScript)
- Express
- CORS
- KnexJS
- MySql
- ts-node
- dotENV

Veja o arquivo package.json




## DESENVOLVEDORES

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/Anderson-Felix-de-Lyra">
        <img src="https://avatars.githubusercontent.com/u/94788717?v=4" width="100px;" alt="Foto do Anderson no GitHub"/><br>
        <sub>
          <b>Anderson Felix</b>
        </sub>
      </a>
    </td>
    <td align="center">
     <a href="https://www.linkedin.com/in/anderson-fl/">
     <img src="https://cdn-icons-png.flaticon.com/64/174/174857.png">
     </a> 
    </td>
  </tr>
</table>



<table>
  <tr>
    <td align="center">
      <a href="https://github.com/FillipeCO">
        <img src="https://avatars.githubusercontent.com/u/87552890?v=4" width="100px;" alt="Foto do Fillipe no GitHub"/><br>
        <sub>
          <b>Fillipe Dias</b>
        </sub>
      </a>
    </td>
    <td align="center">
     <a href="https://www.linkedin.com/in/fillipe-correia/">
     <img src="https://cdn-icons-png.flaticon.com/64/174/174857.png">
     </a> 
    </td>
  </tr>
</table>

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/gdlmartins">
        <img src="https://avatars.githubusercontent.com/u/87931081?v=4" width="100px;" alt="Foto do Gabriel no GitHub"/><br>
        <sub>
          <b>Gabriel Martins</b>
        </sub>
      </a>
    </td>
    <td align="center">
     <a href="https://www.linkedin.com/in/gabriel-martins-71438541/">
     <img src="https://cdn-icons-png.flaticon.com/64/174/174857.png">
     </a> 
    </td>
  </tr>
</table>

<table>
  <tr>
    <td align="center">
      <a href="https://github.com/werruccio">
        <img src="https://avatars.githubusercontent.com/u/94694624?v=4" width="100px;" alt="Foto do Gabriel no GitHub"/><br>
        <sub>
          <b>Wellington Ferruccio</b>
        </sub>
      </a>
    </td>
    <td align="center">
     <a href="https://www.linkedin.com/in/wellington-ferruccio-921a40240/">
     <img src="https://cdn-icons-png.flaticon.com/64/174/174857.png">
     </a> 
    </td>
  </tr>
</table>
